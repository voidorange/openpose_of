//
//  Particle.hpp
//  PoseStudy
//
//  Created by Qinzi Tan on 3/6/19.
//

#ifndef Particle_hpp
#define Particle_hpp

#include "ofMain.h"
#include "Follower.hpp"

class Particle{
    
public:
    ofPoint pos;
    float size;
    ofColor color;
    
    void setup(ofPoint _p);
    void update(ofPoint pos);
    void draw(float _size,float brigntess);
    
    float bornAt;
    
    vector<Follower> followers;
};


#endif /* Particle_hpp */
