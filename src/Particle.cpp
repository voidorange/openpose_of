//
//  Particle.cpp
//  PoseStudy
//
//  Created by Qinzi Tan on 3/6/19.
//

#include "Particle.hpp"
void Particle::setup(ofPoint _p){
    pos = _p * 0.8;
    size = ofRandom(10,50);
//    for (int i = 0; i<20; i++){
//        Follower f;
//        f.init();
//        followers.push_back(f);
//    }
}

void Particle::update(ofPoint _p){
    pos = _p * 0.8;
    size = ofRandom(10,50);
}

void Particle::draw(float _size, float brightness){
    //size = _size;
    //size = ofRandom(_size);
    color = ofColor(brightness,5);
    //color = ofColor::white;
    
    ofPushStyle();
    ofSetColor(color);
    ofSetLineWidth(3);
    //ofNoFill();
    ofDrawCircle(pos, size);
    ofPopStyle();
//
//    for(Follower f : followers){
//        f.updateParticle(pos);
//    }
//
////    for(Follower f : followers){
////        f.drawParticle();
////    }
}
