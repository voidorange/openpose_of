#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFrameRate(60);
    ofSetWindowShape(1300, 908);
    ofBackground(5);
    ofSetCircleResolution(120);
    ofEnableSmoothing();
    ofSetBackgroundAuto(true);
    
    
    //peekaboo
    video.load("afterlight.mp4");
    
    video.setLoopState(OF_LOOP_NORMAL);
    
    for(int i = 0; i<FRAME_COUNTS; i++){
        Frame f;
        
        ofxJSONElement jsonElement;
        
        string file = "output-afterlight/0" + std::to_string(i) + ".json";
        
        bool success = jsonElement.open(file);
        if (success) {
            cout << "json " + std::to_string(i) + " parsed successfully."  << endl;
            fileOpenSuccess = true;
        } else {
            cout << "Failed to parse JSON" << endl;
            fileOpenSuccess = false;
        }
        
        f = parseData(jsonElement);
        
        frames.push_back(f);
    }
    
    
    if (fileOpenSuccess) dataReady = true;
    cout<< "FINISHED PARSE ALL FILES" << endl;
    
    //video.play();

}


//--------------------------------------------------------------
Frame ofApp::parseData(ofxJSONElement json){

    Frame f;
    vector<HumanBody> bodies;
    
    for(int i = 0; i < json["people"].size(); i++){
        HumanBody bodie;
        
        vector<float> xList;
        vector<float> yList;
        
        int counter = 0;
        
        //pose
        if(counter <= 25){
           for(int k = 0; k< json["people"][i]["pose_keypoints_2d"].size(); k+=3){
               float x = json["people"][i]["pose_keypoints_2d"][k].asFloat();
               float y = json["people"][i]["pose_keypoints_2d"][k+1].asFloat();
               string _name = KeypointsOrder[counter];
               ofPoint _pose2D(x,y);

               Keypoint temp;
               temp.name = _name;
               temp.pose2D = _pose2D;

               bodie.keypoints.push_back(temp);
               counter ++;
           }
        }

        //Right hand
//        if(counter <= 25){
//            for(int k = 0; k< json["people"][i]["hand_right_keypoints_2d"].size(); k+=3){
//                float x = json["people"][i]["hand_right_keypoints_2d"][k].asFloat();
//                float y = json["people"][i]["hand_right_keypoints_2d"][k+1].asFloat();
//                string _name = KeypointsOrder[counter];
//                ofPoint _pose2D(x,y);
//
//                Keypoint temp;
//                temp.name = _name;
//                temp.pose2D = _pose2D;
//
//                bodie.keypoints.push_back(temp);
//                counter ++;
//            }
//        }
        
        bodies.push_back(bodie);
    }
    
    f.bodies = bodies;
    
    return f;

}

//--------------------------------------------------------------
void ofApp::update(){
    video.update();
 
    
    if(video.isFrameNew()){
        currentFrame = frames[video.getCurrentFrame()];
        
        if(video.getCurrentFrame() >= 3){
             frameCounter++;
            //particles.clear();
            //particleInEachBody.clear();
//
//            frameCounter=0;
        }
    
        //All Keypoints
        //updateAllKeypoints();

        //arm
        //updateArmInfo();
        
        for(HumanBody b : currentFrame.bodies){
            vector<Particle> _particlesInThisBody;
            _particlesInThisBody.clear();
            
//            for(int i = 0; i < b.keypoints.size(); i++){
//                if(b.keypoints[i].pose2D != ofPoint(0,0)){
//                    Particle _p;
//                    _p.setup( b.keypoints[i].pose2D);
//                    _particlesInThisBody.push_back(_p);
//                }
//            }
            

            //RWrist
            if(b.keypoints[4].pose2D!=ofPoint(0,0)){
            Particle _p1;
            _p1.setup(b.keypoints[4].pose2D);
            _p1.bornAt = ofGetElapsedTimef();
                _particlesInThisBody.push_back(_p1);}
            
            
            //RElbow
            if(b.keypoints[3].pose2D!=ofPoint(0,0)){
            Particle _p;
            _p.setup(b.keypoints[3].pose2D);
            _p.bornAt = ofGetElapsedTimef();
                _particlesInThisBody.push_back(_p);}
//

//
//            //LShoulder
////            Particle _p2;
////            _p2.setup(b.keypoints[5].pose2D);
////            _particlesInThisBody.push_back(_p2);
//
//            //Neck
            if(b.keypoints[1].pose2D!=ofPoint(0,0)){
            Particle _p3;
            _p3.setup(b.keypoints[1].pose2D);
                _particlesInThisBody.push_back(_p3);}
//
//            //LElbow
            if(b.keypoints[6].pose2D!=ofPoint(0,0)){
            Particle _p4;
            _p4.setup(b.keypoints[6].pose2D);
                _particlesInThisBody.push_back(_p4);}
//
            
//            //LWrist
            if(b.keypoints[7].pose2D!=ofPoint(0,0)){
            Particle _p5;
            _p5.setup(b.keypoints[7].pose2D);
                _particlesInThisBody.push_back(_p5);}
//
//            //LShoulder
//            Particle _p6;
//            _p6.setup(b.keypoints[2].pose2D);
//            _particlesInThisBody.push_back(_p6);
            
            
            
            for (int m = 8; m<14; m++){
                if(b.keypoints[m].pose2D != ofPoint(0,0)){
                    Particle _p;
                    _p.setup( b.keypoints[m].pose2D);
                    _p.bornAt = ofGetElapsedTimef();
                    _particlesInThisBody.push_back(_p);

                }
            }

            for (int j = 19; j<22; j++){
                if(b.keypoints[j].pose2D != ofPoint(0,0)){
                    Particle _p;
                    _p.setup( b.keypoints[j].pose2D);
                    _p.bornAt = ofGetElapsedTimef();
                    _particlesInThisBody.push_back(_p);

                }
            }
            
            
            particleInEachBody.push_back(_particlesInThisBody);
        }
        
       
    }
    
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    if(dataReady && !video.isPlaying()) {
        video.play();
    }
    
    //video.draw(0,0, video.getWidth()*0.8, video.getHeight()*0.8);
    
       //Draw in order
        //drawMeshInOrder();
        
        //drawDynamicMesh();
        
        //drawBezier();
        
        //separate body
        drawInSingle();

    
}

//--------------------------------------------------------------
void ofApp::drawInSingle(){
    
//    ofPushStyle();
//    ofSetColor(0, 120);
//    ofDrawRectangle(0,0, video.getWidth(), video.getHeight());
//    ofPopStyle();
    
    
    //BEZIER
    for(int m = frameCounter; m < particleInEachBody.size(); m++){
        if(particleInEachBody[m].size()>5){
        for (int k = 0; k< particleInEachBody[m].size()-4; k++){
                ofPushStyle();
            float b = ofMap(m, 0, particleInEachBody.size() , 0, 255);
            float lineThickness = ofMap(m, 0, particleInEachBody.size(), 2, 4);
            
            ofSetLineWidth(lineThickness);
                ofSetColor(b, 40);
                ofNoFill();
                ofDrawBezier(particleInEachBody[m][k].pos.x, particleInEachBody[m][k].pos.y, particleInEachBody[m][k+1].pos.x, particleInEachBody[m][k+1].pos.y, particleInEachBody[m][k+2].pos.x, particleInEachBody[m][k+2].pos.y, particleInEachBody[m][k+3].pos.x, particleInEachBody[m][k+3].pos.y);
                ofPopStyle();
            
            //particleInEachBody[m][k].draw(ofRandom(10,40),b);

                             
//               float age = ofGetElapsedTimef() - particleInEachBody[m][k].bornAt;
//               if(age < 1){
//                  float b = ofMap(age, 0, 1, 255, 0);
//                  particleInEachBody[m][k].draw(1,b);
//               }

        }
        
    }
        

//        if(particleInEachBody[m].size()>4){
//            for (int k = 0; k< particleInEachBody[m].size()-4; k+=4){
//                Particle temp = particleInEachBody[m][k];
//                //temp.draw(15);
//
//                ofPushStyle();
//                ofSetLineWidth(3);
//                //ofSetColor(ofColor::fromHsb(ofRandom(0,255), 210, 220));
//                ofSetColor(ofColor::white);
//                ofNoFill();
//                ofDrawBezier(particleInEachBody[m][k].pos.x, particleInEachBody[m][k].pos.y,
//                             particleInEachBody[m][k+1].pos.x, particleInEachBody[m][k+1].pos.y,
//                             particleInEachBody[m][k+2].pos.x, particleInEachBody[m][k+2].pos.y,
//                             particleInEachBody[m][k+3].pos.x, particleInEachBody[m][k+3].pos.y);
//
//                ofPopStyle();
//            }

        //}
    }
    
//    float maxLineDis = 120;
//    float minLineDis = 0;
//
//    for(int m = 0; m < particleInEachBody.size(); m++){
//            for (int k = 0; k< particleInEachBody[m].size(); k++){
//                Particle temp = particleInEachBody[m][k];
//
//                for(int j = k+1; j< particleInEachBody[m].size(); j++){
//
//                float dis = ofDist(particleInEachBody[m][k].pos.x, particleInEachBody[m][k].pos.y,particleInEachBody[m][j].pos.x, particleInEachBody[m][j].pos.y);
//
//                    if(dis < maxLineDis && dis > minLineDis){
//                        ofPushStyle();
//                        ofSetLineWidth(3.5);
//                        //ofSetColor(ofColor::fromHsb(ofRandom(0,255), 210, 220));
//                        ofSetColor(ofColor::white);
//                        ofDrawLine(particleInEachBody[m][k].pos.x, particleInEachBody[m][k].pos.y,particleInEachBody[m][j].pos.x, particleInEachBody[m][j].pos.y);
//                        ofPopStyle();
//                    }
//
//                }
//            }
//    }
    
}


//--------------------------------------------------------------
void ofApp::updateArmInfo(){
    
    std::vector<vector<ofPoint>> randomOrderList;
    int keypointCount;
    
    //don't sort
    //vector<ofPoint> upperLimb;
    
    for(HumanBody b : currentFrame.bodies){
        //sort
        vector<ofPoint> upperLimb;
        
        //Rwrist
        upperLimb.push_back(b.keypoints[4].pose2D);
        //RElbow
        upperLimb.push_back(b.keypoints[3].pose2D);
        //RShoulder
        //upperLimb.push_back(b.keypoints[2].pose2D);
        //LShoulder
        //upperLimb.push_back(b.keypoints[5].pose2D);
        //Neck
        upperLimb.push_back(b.keypoints[1].pose2D);
        //LElbow
        upperLimb.push_back(b.keypoints[6].pose2D);
        //LWrist
        upperLimb.push_back(b.keypoints[7].pose2D);

        randomOrderList.push_back(upperLimb);
    }
    
    //Don't sort
//    ofSort(upperLimb, sortPoint);
//        for(int i = 0; i < upperLimb.size(); i++){
//                Particle p;
//                p.update(upperLimb[i]);
//                if(p.pos != ofPoint(0,0,0)){
//                    particles.push_back(p);
//                }
//
//        }
    
    
    //Sort
    ofSort(randomOrderList, sortPointVector);
    for(int i = 0; i < randomOrderList.size(); i++){
        for(int k = 0; k < randomOrderList[i].size(); k++){
            Particle p;
            p.update(randomOrderList[i][k]);
            if(p.pos != ofPoint(0,0,0)){
                particles.push_back(p);
            }
        }
    }
    
}


//--------------------------------------------------------------
void ofApp::updateAllKeypoints(){
//    for(HumanBody b : currentFrame.bodies){
//        for(Keypoint k : b.keypoints){
//            if(k.pose2D != ofPoint(0,0)){
//                Particle p;
//                p.setup( k.pose2D);
//                particles.push_back(p);
//            }
//        }
//    }
    
    for(int i = 0; i<currentFrame.bodies.size(); i++){
        
        for (int m = 0; m<17; m++){
            if(currentFrame.bodies[i].keypoints[m].pose2D != ofPoint(0,0)){
                Particle p;
                p.setup( currentFrame.bodies[i].keypoints[m].pose2D);
                particles.push_back(p);
                
            }
        }
        
        for (int m = 19; m<currentFrame.bodies[i].keypoints.size(); m++){
            if(currentFrame.bodies[i].keypoints[m].pose2D != ofPoint(0,0)){
                Particle p;
                p.setup( currentFrame.bodies[i].keypoints[m].pose2D);
                particles.push_back(p);
                
            }
        }
    }

}


//--------------------------------------------------------------
void ofApp::drawMeshInOrder(){
    if(particles.size() == 0) return;
    
   
    
    for(int i = 0; i<particles.size(); i++){
        particles[i].draw(5,255);
        
        if(i<particles.size()-1){
            ofPushStyle();
            ofSetLineWidth(3);
            //ofSetColor(ofColor::fromHsb(ofRandom(0,255), 210, 220));
            ofSetColor(ofColor::white);
            ofDrawLine(particles[i].pos.x, particles[i].pos.y,particles[i+1].pos.x, particles[i+1].pos.y);
            ofPopStyle();
        }
    }
    
}

//--------------------------------------------------------------
void ofApp::drawBezier(){
    if(particles.size() == 0) return;
    
    for(int i = 0; i<particles.size()-5; i+=4){
//        if(particles[i].pos != ofPoint(0,0)){
//            particles[i].draw(50);
//        }

        ofPushStyle();
        ofSetLineWidth(4);
        //ofSetColor(ofColor::fromHsb(ofRandom(0,255), 210, 220));
        ofSetColor(ofColor::white);
        ofNoFill();
        ofDrawBezier(particles[i].pos.x, particles[i].pos.y, particles[i+1].pos.x, particles[i+1].pos.y, particles[i+2].pos.x, particles[i+2].pos.y, particles[i+3].pos.x, particles[i+3].pos.y);
        
        ofPopStyle();
    }
}

//--------------------------------------------------------------
void ofApp::drawDynamicMesh(){
    if(particles.size() == 0) return;
    
    float maxLineDis = 90;
    float minLineDis = 5;
    
    video.draw(0,0, video.getWidth()*0.8, video.getHeight()*0.8);

    //Brightness Overlay
//    ofPushStyle();
//    ofSetColor(0, 5);
//    ofDrawRectangle(0,0, video.getWidth(), video.getHeight());
//    ofPopStyle();
    
    for(int i = 0; i<particles.size(); i++){
        if(particles[i].pos != ofPoint(0,0)){
            particles[i].draw(50,255);

//            for(int m = i+1; m<particles.size(); m++){
//                float dis = ofDist(particles[i].pos.x, particles[i].pos.y,particles[m].pos.x, particles[m].pos.y);
//                if(dis < maxLineDis && dis > minLineDis){
//                    ofPushStyle();
//                    ofSetLineWidth(3.5);
//                    //ofSetColor(ofColor::fromHsb(ofRandom(0,255), 210, 220));
//                    ofSetColor(ofColor::white);
//                    //ofDrawLine(particles[i].pos.x, particles[i].pos.y,particles[m].pos.x, particles[m].pos.y);
//                    ofPopStyle();
//                }
//
//            }
        }
    }
}






