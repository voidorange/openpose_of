#pragma once

//#define FRAME_COUNTS 459 //circular
//#define FRAME_COUNTS 405 //arms
//#define FRAME_COUNTS 370 //hands
//#define FRAME_COUNTS 313 //dumb
//#define FRAME_COUNTS 326 //dd-02
//#define FRAME_COUNTS 358 //dd-01
#define FRAME_COUNTS 319 //afterlight


#include "ofMain.h"
#include "ofxJSON.h"

#include "Frame.hpp"
#include "HumanBody.hpp"
#include "Keypoint.hpp"
#include "Particle.hpp"
#include "Follower.hpp"


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
    
    void drawBezier();
    void drawDynamicMesh();
    void updateAllKeypoints();
    
    void updateArmInfo();
    
    void drawMeshInOrder();
    void drawInSingle();
    
    static bool sortPointVector(vector<ofPoint> &a, vector<ofPoint> &b){
        return a[0].x < b[0].x;
    }
    
    static bool sortPoint(ofPoint &a, ofPoint &b){
        return a.x < b.x;
    }
    
        Frame parseData(ofxJSONElement json);
	
    string KeypointsOrder[26] = {
        "Nose", //0
        "Neck", //1
        "RShoulder", //2
        "RElbow", //3
        "RWrist", //4
        "LShoulder", //5
        "LElbow", //6
        "LWrist", //7
        "MidHip", //8
        "RHip", //9
        "RKnee", //10
        "RAnkle", //11
        "LHip", //12
        "LKnee", //13
        "LAnkle", //14
        "REye", //15
        "LEye", //16
        "REar", //17
        "LEar", //18
        "LBigToe", //19
        "LSmallToe", //20
        "LHeel", //21
        "RBigToe", //22
        "RSmallToe", //23
        "RHeel", //24
        "Background" //25
        };
    


    vector<Frame> frames;
    bool fileOpenSuccess = true;
    bool dataReady;
    
    Frame currentFrame;
    vector<vector<Particle>> particleInEachBody;
    
    vector<Particle> particles;
    
    ofVideoPlayer video;
    
    int frameCounter;

};

