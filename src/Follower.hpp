//
//  Follower.hpp
//  PoseStudy
//
//  Created by Qinzi Tan on 3/11/19.
//

#ifndef Follower_hpp
#define Follower_hpp


#include "ofMain.h"


class Follower{
    
public:
    void init();
    void updateParticle(ofPoint attractor);
    void drawParticle();
    
    
    float brightness;
    float size;
    
    ofPoint pos;
    ofPoint vel;
    ofPoint pct;
    
    int groupId;
};

#endif /* Follower_hpp */
