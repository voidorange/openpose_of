//
//  Follower.cpp
//  PoseStudy
//
//  Created by Qinzi Tan on 3/11/19.
//

#include "Follower.hpp"

void Follower::init(){
    
    pos.set(ofGetWindowWidth()/2,ofGetWindowHeight()/2);
    
    float randomVel = ofRandom(0.0000005, 0.0001);
    //float randomVel = ofRandom(0.2,0.5);
    vel.set(randomVel,randomVel);
    
    brightness = ofMap(randomVel,0.0000005, 0.0001, 90, 255);
    size = ofMap(randomVel,0.0000005, 0.0001, 0,2);
}

//--------------------------------------------------------------
void Follower::updateParticle(ofPoint _attractor){
    
    pct += vel;
    ofPoint dest = _attractor;
    
    pos = (ofPoint(1,1)-pct) * pos + pct * dest;
    //pos *= 0.8;
}

//--------------------------------------------------------------
void Follower::drawParticle(){
    ofSetColor(brightness,3);
    ofDrawCircle(pos, size);
}
