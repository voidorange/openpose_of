//
//  Keypoint.hpp
//  PoseStudy
//
//  Created by Qinzi Tan on 3/6/19.
//

#ifndef Keypoint_hpp
#define Keypoint_hpp

#include "ofMain.h"

class Keypoint{
public:
    string name;
    ofPoint pose2D;
};

#endif /* Keypoint_hpp */
