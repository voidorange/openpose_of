//
//  Frame.hpp
//  PoseStudy
//
//  Created by Qinzi Tan on 3/6/19.
//

#ifndef Frame_hpp
#define Frame_hpp

#include "ofMain.h"
#include "HumanBody.hpp"

class Frame{
public:
    vector<HumanBody> bodies;
};

#endif /* Frame_hpp */
