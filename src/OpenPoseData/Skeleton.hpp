//
//  Skeleton.hpp
//  PoseStudy
//
//  Created by Qinzi Tan on 3/6/19.
//

#ifndef Skeleton_hpp
#define Skeleton_hpp

#include "ofMain.h"

class Skeleton{
public:
    vector<ofPoint> joints;
};

#endif /* Skeleton_hpp */
