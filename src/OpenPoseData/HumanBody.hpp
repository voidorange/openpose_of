//
//  HumanBody.hpp
//  PoseStudy
//
//  Created by Qinzi Tan on 3/6/19.
//

#ifndef HumanBody_hpp
#define HumanBody_hpp

#include "ofMain.h"
#include "Keypoint.hpp"

class HumanBody{
public:
    vector<Keypoint> keypoints;
};

#endif /* HumanBody_hpp */
